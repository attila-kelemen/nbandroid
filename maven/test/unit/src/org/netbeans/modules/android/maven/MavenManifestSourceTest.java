package org.netbeans.modules.android.maven;

import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;
import java.io.File;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.junit.MockServices;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;
import org.netbeans.modules.android.project.spi.DalvikPlatformResolver;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Ignore;
import org.netbeans.modules.android.project.FileUtilities;
import org.netbeans.modules.android.project.api.AndroidManifestSource;
import org.netbeans.modules.android.project.spi.AndroidDebugInfo;

/**
 *
 * @author radim
 */
@Ignore
public class MavenManifestSourceTest {
  private static final Logger LOG = Logger.getLogger(MavenManifestSourceTest.class.getName());
  
  public MavenManifestSourceTest() {
  }

  private static final String SDK_DIR = System.getProperty("test.all.android.sdks.home");
  private static final String MAVEN_DIR = System.getProperty("test.all.maven.home");
  private static File tempFolder;
  private static FileObject projdir;
  private static Project pp;
  private static FileObject scratch;

  @BeforeClass
  public static void setUpClass() throws Exception {
    MockServices.setServices();
    DalvikPlatformManager.getDefault().setSdkLocation(SDK_DIR);
  }
  
  @Before
  public void setup() throws Exception {
    tempFolder = File.createTempFile("junit", "");
    tempFolder.delete();
    tempFolder.mkdir();

    scratch = FileUtil.toFileObject(tempFolder);
    // FileObject sdkDirFo = FileUtil.toFileObject(new File(SDK_DIR));
  }

  private void createProject(String archetypeVersion, String platform) throws Exception {
    File mvn = new File(MAVEN_DIR, "bin/mvn"); // TODO add .exe on Windows
    ProcessBuilder pb = new ProcessBuilder(Lists.newArrayList(
            mvn.getAbsolutePath(),
            "--batch-mode",
            "archetype:generate",
            "-DarchetypeArtifactId=android-quickstart",
            "-DarchetypeGroupId=de.akquinet.android.archetypes",
            archetypeVersion,
            "-DgroupId=your.company",
            "-DartifactId=my-android-application",
            platform)).
        directory(tempFolder).redirectErrorStream(true);
    pb.environment().put("JAVA_HOME", System.getProperty("java.home"));
    final Process p = pb.start();
    Executors.newCachedThreadPool().submit(new Callable<Void>() {

      @Override
      public Void call() throws Exception {
        ByteStreams.copy(p.getInputStream(), System.out);
        return null;
      }
    }).get(60, TimeUnit.SECONDS);
    p.waitFor();
    
    FileUtil.refreshFor(tempFolder);
    projdir = scratch.getFileObject("my-android-application");
    pp = ProjectManager.getDefault().findProject(projdir);
  }

  @After
  public void delete() {
    FileUtilities.recursiveDelete(tempFolder);
  }

  @Test
  public void manifest() throws Exception {
    createProject("-DarchetypeVersion=1.0.9", "-Dplatform.version=4.1.1.4");
    assertNotNull(pp);
    AndroidManifestSource ms = pp.getLookup().lookup(AndroidManifestSource.class);
    assertNotNull(ms);
    assertEquals(projdir.getFileObject("AndroidManifest.xml"), ms.get());
  }
}
