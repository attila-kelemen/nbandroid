package org.nbandroid.netbeans.gradle;

import com.android.builder.model.AndroidProject;
import com.google.common.collect.ImmutableList;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import org.gradle.tooling.model.gradle.GradleBuild;
import org.jtrim2.collections.CollectionsEx;
import org.jtrim2.utils.LazyValues;
import org.netbeans.api.project.Project;
import org.netbeans.gradle.project.api.entry.GradleProjectExtension2;
import org.netbeans.gradle.project.api.entry.GradleProjectExtensionDef;
import org.netbeans.gradle.project.api.entry.ModelLoadResult;
import org.netbeans.gradle.project.api.entry.ParsedModel;
import org.netbeans.gradle.project.api.modelquery.GradleModelDefQuery1;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author radim
 */
@ServiceProvider(service = GradleProjectExtensionDef.class, position = 900)
public class AndroidGradleProjectExtQuery implements GradleProjectExtensionDef<AndroidGradleModels> {
  private static final List<Class<?>> REQUIRED_MODELS = ImmutableList.of(GradleBuild.class, AndroidProject.class);

  private final Supplier<Lookup> lookupRef;

  public AndroidGradleProjectExtQuery() {
    this.lookupRef = LazyValues.lazyValue(() -> {
      return Lookups.fixed((GradleModelDefQuery1) (target) -> REQUIRED_MODELS);
    });
  }

  @Override
  public String getName() {
    return AndroidGradleExtension.class.getName();
  }

  @Override
  public String getDisplayName() {
    return "NB Android";
  }

  @Override
  public Lookup getLookup() {
    return lookupRef.get();
  }

  @Override
  public Class<AndroidGradleModels> getModelType() {
    return AndroidGradleModels.class;
  }

  private static AndroidGradleModels tryParseFromLookup(Lookup lookup) {
    GradleBuild gradleBuild = lookup.lookup(GradleBuild.class);
    if (gradleBuild == null) {
      return null;
    }

    AndroidProject androidProject = lookup.lookup(AndroidProject.class);
    if (androidProject == null) {
      return null;
    }

    return new AndroidGradleModels(gradleBuild, androidProject);
  }

  @Override
  public ParsedModel<AndroidGradleModels> parseModel(ModelLoadResult modelResult) {
    AndroidGradleModels mainModels = tryParseFromLookup(modelResult.getMainProjectModels());

    Map<File, Lookup> evaluatedModels = modelResult.getEvaluatedProjectsModel();
    Map<File, AndroidGradleModels> otherModels = CollectionsEx.newHashMap(evaluatedModels.size());

    evaluatedModels.forEach((projectDir, lookup) -> {
      AndroidGradleModels projectModels = tryParseFromLookup(lookup);
      otherModels.put(projectDir, projectModels);
    });

    return new ParsedModel<>(mainModels, otherModels);
  }

  @Override
  public GradleProjectExtension2<AndroidGradleModels> createExtension(Project project) throws IOException {
    AndroidGradleExtension ext = new AndroidGradleExtension(project);
    for (AndroidGradleCallback callback : Lookup.getDefault().lookupAll(AndroidGradleCallback.class)) {
      callback.customize(ext, project);
    }
    return ext;
  }

  @Override
  public Set<String> getSuppressedExtensions() {
    return Collections.emptySet();
  }
}
