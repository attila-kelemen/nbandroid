package org.nbandroid.netbeans.gradle;

import com.android.builder.model.AndroidProject;
import org.netbeans.api.project.Project;
import org.netbeans.modules.android.project.api.AndroidClassPath;
import org.netbeans.spi.project.ui.ProjectOpenedHook;

final class GradleProjectOpenedHook extends ProjectOpenedHook implements AndroidModelAware {

  private final Project project;

  GradleProjectOpenedHook(Project project) {
    this.project = project;
  }

  private AndroidClassPath classpath() {
    return project.getLookup().lookup(AndroidClassPath.class);
  }

  @Override 
  protected void projectOpened() {
    AndroidClassPath cp = classpath();
    if (cp != null) {
      cp.register();
    }
  }

  @Override
  protected void projectClosed() {
    AndroidClassPath cp = classpath();
    if (cp != null) {
      cp.unregister();
    }
  }

  @Override
  public void setAndroidProject(AndroidProject aPrj) {
    AndroidClassPath cp = classpath();
    if (cp != null) {
      cp.register();
    }
    
  }

}
