package org.nbandroid.netbeans.gradle;

import com.android.builder.model.AndroidProject;
import java.io.Serializable;
import java.util.Objects;
import org.gradle.tooling.model.gradle.GradleBuild;

public final class AndroidGradleModels implements Serializable{
  private static final long serialVersionUID = 1L;

  private final GradleBuild gradleBuild;
  private final AndroidProject androidProject;

  public AndroidGradleModels(GradleBuild gradleBuild, AndroidProject androidProject) {
    this.gradleBuild = Objects.requireNonNull(gradleBuild, "gradleBuild");
    this.androidProject = Objects.requireNonNull(androidProject, "androidProject");
  }

  public GradleBuild getGradleBuild() {
    return gradleBuild;
  }

  public AndroidProject getAndroidProject() {
    return androidProject;
  }
}
