package org.nbandroid.netbeans.gradle;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.nbandroid.netbeans.gradle.ui.DependenciesNode;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.project.spi.DalvikPlatformResolver;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

import org.netbeans.api.java.project.JavaProjectConstants;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.Sources;
import org.netbeans.junit.MockServices;
import org.netbeans.modules.android.project.api.AndroidConstants;
import org.netbeans.modules.android.project.api.AndroidProjects;
import org.netbeans.modules.android.project.ui.PlatformNode;
import org.netbeans.spi.project.ui.LogicalViewProvider;
import org.openide.nodes.Node;

import static org.nbandroid.netbeans.gradle.GradleProjectFixture.*;
import org.openide.util.Exceptions;

/**
 *
 * @author radim
 */
public class BasicWithTestDepsTest {
  private static final Logger LOG = Logger.getLogger(BasicWithTestDepsTest.class.getName());
  private static final String PRJ_NAME = "basicWithTestDeps";

  private static PluginsFixture pluginsFix;
  private static GradleProjectFixture prjFix;
  private static FileObject foProjectTestSrc;

  @BeforeClass
  public static void setUpClass() throws Exception {
    MockServices.setServices();
    pluginsFix = new PluginsFixture().setupSDK().setupGradle();

    prjFix = new GradleProjectFixture.Builder()
        .useName("basicWithTestDeps")
        .useIs(BasicWithTestDepsTest.class.getResourceAsStream("basicWithTestDeps.zip"))
        .useProjectCustomizeAction(new ProjectCustomizeAction() {
          @Override public void customize(File projectDir) {
            try {
              Files.asByteSink(new File(projectDir, "build.gradle"))
                  .writeFrom(BasicWithTestDepsTest.class.getResourceAsStream("basicWithTestDeps.gradle.sample"));
            } catch (IOException ex) {
              throw new RuntimeException(ex);
            }
          }
        })
        .create();

    foProjectTestSrc = prjDir(prjFix).getFileObject("src/androidTest/java");
  }


  @AfterClass
  public static void clear() {
    prjFix.tearDown();
  }

  @Test
  public void isAndroidProject() throws Exception {
    assertTrue(AndroidProjects.isAndroidProject(prjFix.prj));
  }

  @Test
  public void basic() throws Exception {
    DalvikPlatformResolver platformProvider = prjLookup(prjFix).lookup(DalvikPlatformResolver.class);
    assertNotNull(platformProvider);
    DalvikPlatform platform = platformProvider.findDalvikPlatform();
    assertNotNull(platform);
  }

  @Test
  public void testSourcePath() throws Exception {
    // get the classpath
    verifyClasspath(prjFix, foProjectTestSrc, ClassPath.SOURCE, PRJ_NAME + "/src/androidTest/java");
    verifyClasspath(prjFix, foProjectTestSrc, ClassPath.BOOT, "android.jar");

    //Clean and wait
    prjFix.cleanAction().invoke();
    // get the classpath
    verifyClasspath(prjFix, foProjectTestSrc, ClassPath.SOURCE, PRJ_NAME + "/src/androidTest/java");
    verifyClasspath(prjFix, foProjectTestSrc, ClassPath.BOOT, "android.jar");

    //Build and wait
    prjFix.action(AndroidConstants.COMMAND_BUILD_TEST)
        .andWaitForFile(new File(FileUtil.toFile(prjDir(prjFix)), "build/generated/source/buildConfig/test/debug"))
        .invoke();
    // get the classpath
    verifyClasspath(prjFix, foProjectTestSrc, ClassPath.SOURCE,
        PRJ_NAME + "/src/androidTest/java", PRJ_NAME + "/build/generated/source/buildConfig/test/debug");
    verifyClasspath(prjFix, foProjectTestSrc, ClassPath.COMPILE,
        PRJ_NAME + "/build/intermediates/classes/debug",
        "extras/android/m2repository/com/android/support/support-v13/13.0.0/support-v13-13.0.0.jar",
        "extras/android/m2repository/com/android/support/support-v4/13.0.0/support-v4-13.0.0.jar",
        PRJ_NAME + "/build/intermediates/exploded-aar/com.google.android.gms/play-services/3.1.36/classes.jar",
        "mockito-core-1.9.5.jar",
        "dexmaker-1.0.jar",
        "dexmaker-mockito-1.0.jar"
        /*, following looks like implementation detail in Gradle
        "basic/build/dependency-cache/debug"*/);
    verifyClasspath(prjFix, foProjectTestSrc, ClassPath.BOOT, "android.jar");
  }

  @Test
  public void sources() throws Exception {
    Sources sources = ProjectUtils.getSources(prjFix.prj);
    assertNotNull(sources);

    final FileObject foSrc = prjDir(prjFix).getFileObject("src/main/java/com/android/tests/basic/Main.java");
    SourceGroup[] sourceGroups = sources.getSourceGroups(JavaProjectConstants.SOURCES_TYPE_JAVA);
    assertTrue("java source in " + sources,
        Iterables.any(
            Arrays.asList(sourceGroups),
            sourceGroupContainsFile(foSrc)));
    final FileObject foRes = prjDir(prjFix).getFileObject("src/main/res/layout/main.xml");
    SourceGroup[] sourceGroups2 = sources.getSourceGroups(AndroidConstants.SOURCES_TYPE_ANDROID_RES);
    assertTrue("app resource in " + sources,
        Iterables.any(
            Arrays.asList(sourceGroups2),
            sourceGroupContainsFile(foRes)));
  }

  @Test
  public void nodes() {
    LogicalViewProvider viewProvider = prjLookup(prjFix).lookup(LogicalViewProvider.class);
    assertNotNull(viewProvider);
    Node prjNode = viewProvider.createLogicalView();
    Node[] nodes = prjNode.getChildren().getNodes(true);
    Node depsNode = Iterables.find(Lists.newArrayList(nodes), new Predicate<Node>() {
      @Override
      public boolean apply(Node t) {
        return "Dependencies".equals(t.getDisplayName());
      }
    });
    assertTrue(depsNode instanceof DependenciesNode);
    Node platformNode = Iterables.find(
        Lists.newArrayList(depsNode.getChildren().getNodes(true)),
        new Predicate<Node>() {
          @Override
          public boolean apply(Node t) {
            return "Android 4.0.3".equals(t.getDisplayName());
          }
        });
    assertTrue(platformNode instanceof PlatformNode);
  }
}