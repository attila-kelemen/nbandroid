package org.nbandroid.netbeans.gradle;

import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nbandroid.netbeans.gradle.config.BuildVariant;
import org.netbeans.api.java.classpath.ClassPath;
import org.openide.filesystems.FileObject;
import org.netbeans.modules.android.project.api.AndroidProjects;

import static org.junit.Assert.*;
import static org.nbandroid.netbeans.gradle.GradleProjectFixture.*;

public class FlavoredGradleProjectTest {
  private static final Logger LOG = Logger.getLogger(DependenciesGradleProjectTest.class.getName());
  
  private static PluginsFixture pluginsFix;
  private static GradleProjectFixture prjFix;
  private static FileObject foProjectSrc;

  @BeforeClass
  public static void setUpClass() throws Exception {
    pluginsFix = new PluginsFixture().setupSDK().setupGradle();
    prjFix = new GradleProjectFixture.Builder()
        .usePrefix(GradleTests.TEST_ARCHIVE_DIR)
        .useName("flavored")
        .useIs(GradleTests.testArchiveStream())
        .create();
    
    foProjectSrc = prjDir(prjFix).getFileObject("src/main/java");
  }

  @AfterClass
  public static void clear() {
    prjFix.tearDown();
  }

  @Test
  public void isAndroidProject() throws Exception {
    assertTrue(AndroidProjects.isAndroidProject(prjFix.prj));
  }
  
  @Test
  public void testSourcePath() throws Exception {
    BuildVariant bv = prjLookup(prjFix).lookup(BuildVariant.class);
    bv.setVariantName("f1Debug");
    BuildVariant.RP.post(new Runnable() { @Override public void run() {}}).waitFinished();
    // get the classpath
    verifyClasspath(prjFix, prjDir(prjFix).getFileObject("src/androidTest/java"), ClassPath.SOURCE, 
        "flavored/src/androidTest/java");
    bv.setVariantName("f2Debug");
    BuildVariant.RP.post(new Runnable() { @Override public void run() {}}).waitFinished();
    // get the classpath
    verifyClasspath(prjFix, prjDir(prjFix).getFileObject("src/androidTest/java"), ClassPath.SOURCE, 
        "flavored/src/androidTest/java",
        "flavored/src/androidTestF2/java");
  }
}