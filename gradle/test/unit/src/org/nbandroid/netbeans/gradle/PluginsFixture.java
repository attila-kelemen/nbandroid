package org.nbandroid.netbeans.gradle;

import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.gradle.project.properties.GradleLocationDef;
import org.netbeans.gradle.project.properties.GradleLocationDirectory;
import org.netbeans.gradle.project.properties.PlatformSelectionMode;
import org.netbeans.gradle.project.properties.ScriptPlatform;
import org.netbeans.gradle.project.properties.global.CommonGlobalSettings;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;

/**
 *
 * @author radim
 */
public class PluginsFixture {
  private static final String SDK_DIR = System.getProperty("test.all.android.sdks.home");
  private static final String GRADLE_DIR = System.getProperty("test.all.gradle.home");

  private DalvikPlatformManager platformManager;

  public PluginsFixture setupGradle() {
    GradleLocationDirectory.getLocationRef(GRADLE_DIR);
    CommonGlobalSettings.getDefault().gradleLocation().setValue(new GradleLocationDef(
        GradleLocationDirectory.getLocationRef(GRADLE_DIR),
        false));
    CommonGlobalSettings.getDefault().defaultJdk().setValue(new ScriptPlatform(
        JavaPlatform.getDefault(),
        PlatformSelectionMode.BY_LOCATION));
    return this;
  }

  public PluginsFixture setupSDK() {
    platformManager = DalvikPlatformManager.getDefault();
    platformManager.setSdkLocation(SDK_DIR);
    return this;
  }

  public DalvikPlatformManager getPlatformManager() {
    return platformManager;
  }
}
