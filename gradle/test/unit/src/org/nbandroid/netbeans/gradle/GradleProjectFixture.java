package org.nbandroid.netbeans.gradle;

import com.android.builder.model.AndroidProject;
import com.google.common.base.Charsets;
import com.google.common.base.Predicate;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import org.nbandroid.netbeans.gradle.query.GradleAndroidClassPathProvider;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.gradle.project.NbGradleProject;
import org.netbeans.gradle.project.NbGradleProjectFactory;
import org.netbeans.gradle.project.model.NbGradleModel;
import org.netbeans.modules.android.project.FileUtilities;
import org.netbeans.modules.android.project.TestUtils;
import org.netbeans.spi.project.ActionProvider;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

import static org.junit.Assert.*;
import org.netbeans.api.project.SourceGroup;
import org.openide.nodes.Node;
/**
 *
 * @author radim
 */
public class GradleProjectFixture {
  private static final int MAX_RETRY_COUNT =
      Integer.getInteger(AndroidGradleProjectExtQueryTest.class.getSimpleName()+".action_timeout", 100);
  private static final String SDK_DIR = System.getProperty("test.all.android.sdks.home");
  private static final String GRADLE_DIR = System.getProperty("test.all.gradle.home");

  public static class Builder {
    private String prefix;
    private String name;
    private InputStream is;
    private ProjectCustomizeAction action;

    public Builder usePrefix(String prefix) {
      this.prefix = prefix;
      return this;
    }

    public Builder useName(String name) {
      this.name = name;
      return this;
    }

    public Builder useIs(InputStream is) {
      this.is = is;
      return this;
    }

    public Builder useProjectCustomizeAction(ProjectCustomizeAction action) {
      this.action = action;
      return this;
    }

    GradleProjectFixture create() throws Exception {
      return new GradleProjectFixture(prefix != null ? (prefix + File.separatorChar + name) : name, is, action);
    }
  }

  public interface ProjectCustomizeAction {
    void customize(File projectDir);
  }

  public static FileObject prjDir(GradleProjectFixture prjFix) {
    return prjFix.prj.getProjectDirectory();
  }

  public static Lookup prjLookup(GradleProjectFixture prjFix) {
    return prjFix.prj.getLookup();
  }

  public static Predicate<SourceGroup> sourceGroupContainsFile(final FileObject fo) {
    return new Predicate<SourceGroup>() {
      @Override
      public boolean apply(SourceGroup t) {
        return t.contains(fo);
      }
    };
  }

  public static Node subNodeByDisplayName(Node node, final String displayName) {
    Node[] nodes = node.getChildren().getNodes(true);
    Node subNode = Iterables.find(Lists.newArrayList(nodes), new Predicate<Node>() {
      @Override
      public boolean apply(Node t) {
        return displayName.equals(t.getDisplayName());
      }
    }, null);
    return subNode;
  }

  private File tempFolder;
  public final File prjDir;
  public final Project prj;

  private GradleProjectFixture(String prjPath, InputStream is, ProjectCustomizeAction action) throws Exception {
    tempFolder = File.createTempFile("junit", "");
    tempFolder.delete();
    tempFolder.mkdir();
    TestUtils.unzip(is, tempFolder);
    prjDir = new File(tempFolder, prjPath);
    Files.write("sdk.dir=" + SDK_DIR, new File(prjDir, "local.properties"), Charsets.UTF_8);
    NbGradleProjectFactory.safeToOpen(prjDir);

    if (action != null) {
      action.customize(prjDir);
    }

    prj = ProjectManager.getDefault().findProject(FileUtil.toFileObject(prjDir));
    NbGradleProject gPrj = prj.getLookup().lookup(NbGradleProject.class);
    assertNotNull(prj);
    AndroidGradleExtension ext = gPrj.getLookup().lookup(AndroidGradleExtension.class);
    assertNotNull(ext);
    NbGradleModel model = gPrj.currentModel().getValue();
    ext.loadedSignal.await(150, TimeUnit.SECONDS);
  }

  public void tearDown() {
    FileUtilities.recursiveDelete(tempFolder);
  }

  public Project loadProject(String relativePath) throws Exception {
    FileObject subPrjDir = prj.getProjectDirectory().getFileObject(relativePath);
    NbGradleProjectFactory.safeToOpen(subPrjDir);
    Project subPrj = ProjectManager.getDefault().findProject(subPrjDir);
    NbGradleProject gPrj = subPrj.getLookup().lookup(NbGradleProject.class);
    assertNotNull(subPrj);
    AndroidGradleExtension prjExt = subPrj.getLookup().lookup(AndroidGradleExtension.class);
    assertNotNull(prjExt);
    NbGradleModel model = gPrj.currentModel().getValue();
    prjExt.loadedSignal.await(150, TimeUnit.SECONDS);
    return subPrj;
  }

  public GradleActionRunner action(String actionName) {
    return new GradleActionRunner(actionName);
  }

  public GradleActionRunner buildAction() {
    return action(ActionProvider.COMMAND_BUILD)
        .andWaitForFile(new File(prjDir, "build/generated/source/r/debug"));
  }

  public GradleActionRunner cleanAction() {
    return action(ActionProvider.COMMAND_CLEAN)
        .andWaitForFileDelete(new File(prjDir, "build"));
  }

  public class GradleActionRunner {

    private final String actionName;
    private final boolean filePresent;
    private final File checkedFile;

    private GradleActionRunner(String actionName) {
      this(actionName, false, null);
    }

    public GradleActionRunner(String actionName, boolean filePresent, File checkedFile) {
      this.actionName = actionName;
      this.filePresent = filePresent;
      this.checkedFile = checkedFile;
    }

    public GradleActionRunner andWaitForFile(File output) {
      return new GradleActionRunner(actionName, true, output);
    }

    public GradleActionRunner andWaitForFileDelete(File output) {
      return new GradleActionRunner(actionName, false, output);
    }

    public void invoke() throws InterruptedException {
      final ActionProvider ap = prj.getLookup().lookup(ActionProvider.class);
      assertTrue(Arrays.asList(ap.getSupportedActions()).contains(actionName));
      assertTrue(ap.isActionEnabled(actionName, Lookups.fixed(prj)));
      ap.invokeAction(actionName, Lookups.fixed(prj));
      if (checkedFile != null) {
        for (int i = 0; !(checkedFile.exists() == filePresent); i++) {
          if (i == MAX_RETRY_COUNT) {
            throw new AssertionError("Time out while waiting for " + checkedFile + " (to be " + (filePresent ? "present)" : "removed)"));
          }
          Thread.sleep(TimeUnit.SECONDS.toMillis(1));
        }
      }

    }
  }

  public static void verifyClasspath(GradleProjectFixture prjFix, FileObject fo, String cpType, String... entries) {
    verifyClasspath(prjFix.prj, fo, cpType, entries);
  }

  public static void verifyClasspath(Project prj, FileObject fo, String cpType, String... entries) {
    AndroidGradleExtension ext = prj.getLookup().lookup(AndroidGradleExtension.class);
    AndroidProject ap = ext.aPrj;
    assertNotNull(ap);
    assertNotNull(ext.gradleBuild);

    GradleAndroidClassPathProvider cpp = prj.getLookup().lookup(GradleAndroidClassPathProvider.class);
    cpp.setAndroidProject(ap);

    ClassPath classpath = cpp.findClassPath(fo, cpType);
    assertNotNull("classpath " + cpType + " found", classpath);

    for (final String entry : entries) {
      assertTrue(
          "classpath " + classpath + " contains entry " + entry,
          Iterables.any(
              Splitter.on(':').split(classpath.toString()),
              new Predicate<String>() {

                @Override
                public boolean apply(String t) {
                  return t.endsWith(entry);
                }
              }));
    }
    // TODO this is not working in tests (missing dependency on cpprovider delegating to projects?)
    // ClassPath classpath2 = ClassPath.getClassPath(fo, cpType);
    // assertNotNull("classpath " + cpType + " found", classpath2);
  }
}
