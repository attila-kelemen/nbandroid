/*
 * Copyright 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.nbandroid.multitestrunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Runs the target test class against the versions specified in a {@link TargetVersions}
 */
public class MultiVersionSpecRunner extends AbstractMultiTestRunner {

  public MultiVersionSpecRunner(Class<?> target) {
    super(target);
  }

  @Override
  protected void createExecutions() {
    TargetVersions versions = target.getAnnotation(TargetVersions.class);
    if (versions != null) {
      for (String version : versions.value()) {
        add(new VersionExecution(version));
      }
    } else {
      throw new RuntimeException("Target class " + target.getName() + " is not annotated with @TargetVersions.");
    }
  }

  private static class VersionExecution extends AbstractMultiTestRunner.Execution {

    private final String version;

    VersionExecution(String version) {
      this.version = version;
    }

    @Override
    protected String getDisplayName() {
      return version;
    }

    @Override
    protected void before() {
      try {
        Method method = target.getMethod("setVersion", String.class);
        method.invoke(null, version);
      } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
        throw new RuntimeException("Cannot set version to target class " + target.getName() + " .", ex);
      }
    }
  }
}
