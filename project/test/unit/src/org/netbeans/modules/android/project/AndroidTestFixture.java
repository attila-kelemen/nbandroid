/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netbeans.modules.android.project;

import com.google.common.base.Charsets;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import java.io.File;
import java.util.Map;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.junit.MockServices;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class AndroidTestFixture {

  public static final String SDK_DIR = System.getProperty("test.all.android.sdks.home");

  public static AndroidTestFixture create() throws Exception {
    return new AndroidTestFixture().setUp();
  }
  
  private File tempFolder;
  private FileObject scratch;
  
  private FileObject sdkDirFo;
  
  private Map<String, FileObject> projFolders = Maps.newHashMap();
  private Map<String, Project> projects = Maps.newHashMap();

  private AndroidTestFixture setUp() throws Exception {
    MockServices.setServices();
    DalvikPlatformManager.getDefault().setSdkLocation(SDK_DIR);
    tempFolder = File.createTempFile("junit", "");
    tempFolder.delete();
    tempFolder.mkdir();

    scratch = FileUtil.toFileObject(tempFolder);
    sdkDirFo = FileUtil.toFileObject(new File(SDK_DIR));

    return this;
  }

  public AndroidTestFixture withProject(String prjFolderName, String sdkPath) throws Exception {
    return withProject(prjFolderName, sdkPath, false);
  }

  public AndroidTestFixture withTestProject(String prjFolderName, String sdkPath) throws Exception {
    return withProject(prjFolderName, sdkPath, true);
  }

  private AndroidTestFixture withProject(String prjFolderName, String sdkPath, boolean test) throws Exception {
    FileObject projdir = scratch.createFolder(prjFolderName);
    FileUtilities.recursiveCopy(sdkDirFo.getFileObject(sdkPath), projdir);
    if (test) {
      FileObject testPropsFO = projdir.createData("ant.properties");
      Files.write("tested.project.dir=../" + prjFolderName.substring(0, prjFolderName.length() - 4)+ "\n", 
          FileUtil.toFile(testPropsFO), Charsets.UTF_8);
      testPropsFO.refresh();
    }

    Project pp = ProjectManager.getDefault().findProject(projdir);
    projFolders.put(prjFolderName, projdir);
    projects.put(prjFolderName, pp);
    return this;
  }

  public void tearDown() {
    FileUtilities.recursiveDelete(tempFolder);
  }
  
  public FileObject getProjectFolder(String prjFolderName) {
    return projFolders.get(prjFolderName);
  }
  
  public Project getProject(String prjFolderName) {
    return projects.get(prjFolderName);
  }

  public File getTempFolder() {
    return tempFolder;
  }
}
