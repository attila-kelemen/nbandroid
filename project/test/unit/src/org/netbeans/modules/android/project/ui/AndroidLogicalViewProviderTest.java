/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netbeans.modules.android.project.ui;

import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import javax.swing.Action;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.spi.project.ui.LogicalViewProvider;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Node;
import org.netbeans.modules.android.project.AndroidProject;
import org.netbeans.modules.android.project.AndroidTestFixture;
import static org.junit.Assert.*;

public class AndroidLogicalViewProviderTest {

  private static AndroidTestFixture fixture;

  private static FileObject projdir;

  private static FileObject someSource1;

  @BeforeClass
  public static void setUpClass() throws Exception {
    fixture = AndroidTestFixture.create().withProject("Snake", "samples/android-8/Snake");
    projdir = fixture.getProjectFolder("Snake");

    someSource1 = projdir.getFileObject("src/com/example/android/snake/Snake.java");
  }


  @AfterClass
  public static void delete() {
    fixture.tearDown();
  }

  @Test
  public void findPath() throws Exception {
    final AndroidProject proj = (AndroidProject) ProjectManager.getDefault().findProject(projdir);
    assertNotNull(someSource1);

    LogicalViewProvider viewProvider = proj.getLookup().lookup(LogicalViewProvider.class);
    assertNotNull(viewProvider);
    Node rootNode = viewProvider.createLogicalView();
    assertNotNull(rootNode);
    Node subNode = viewProvider.findPath(rootNode, someSource1);
    assertNotNull(subNode);
    FileObject layout = projdir.getFileObject("res/layout/snake_layout.xml");
    assertNotNull(layout);
    subNode = viewProvider.findPath(rootNode, layout);
    assertNotNull(subNode);
  }

  @Test
  public void previewAction() throws Exception {
    final AndroidProject proj = (AndroidProject) ProjectManager.getDefault().findProject(projdir);
    assertNotNull(someSource1);

    LogicalViewProvider viewProvider = proj.getLookup().lookup(LogicalViewProvider.class);
    assertNotNull(viewProvider);
    Node rootNode = viewProvider.createLogicalView();
    assertNotNull(rootNode);
    FileObject layout = projdir.getFileObject("res/layout/snake_layout.xml");
    assertNotNull(layout);
    Node subNode = viewProvider.findPath(rootNode, layout);
    // has preview action
    Action[] actions = subNode.getActions(false);
    assertTrue(Iterables.any(Lists.newArrayList(actions), Predicates.instanceOf(PreviewLayoutAction.class)));

    // not on folder
    actions = subNode.getParentNode().getActions(false);
    assertFalse(Iterables.any(Lists.newArrayList(actions), Predicates.instanceOf(PreviewLayoutAction.class)));
  }
}
