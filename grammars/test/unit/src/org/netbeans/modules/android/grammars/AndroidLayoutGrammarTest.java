/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netbeans.modules.android.grammars;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.io.Resources;
import java.util.List;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;
import org.netbeans.modules.android.project.api.ReferenceResolver;
import org.netbeans.modules.android.project.api.ResourceRef;
import org.nbandroid.netbeans.test.AbstractAndroidPlatformTest;
import org.nbandroid.netbeans.test.PlatformVersions;

import static org.junit.Assert.*;

/**
 * Test functionality of AndroidGrammar.
 */
@PlatformVersions
public class AndroidLayoutGrammarTest extends AbstractAndroidPlatformTest {

  private static final String SDK_DIR = System.getProperty("test.all.android.sdks.home");

  private static final String PRELUDE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
  private static final String XMLNS = " xmlns:android=\"http://schemas.android.com/apk/res/android\"\n";
  private static final String LAYOUT_ATTRS = "    android:orientation=\"vertical\""
      + "    android:layout_width=\"fill_parent\""
      + "    android:layout_height=\"fill_parent\">";

  private static ReferenceResolver rr;

  @BeforeClass
  public static void setUp() throws Exception {
    DalvikPlatformManager.getDefault().setSdkLocation(SDK_DIR);
    rr = new ReferenceResolver() {

      @Override
      public Iterable<ResourceRef> getReferences() {
        return Lists.newArrayList(
            new ResourceRef(true, "com.example.android.snake", "id", "snake", Integer.valueOf(1)),
            new ResourceRef(true, "com.example.android.snake", "id", "text", Integer.valueOf(2)),
            new ResourceRef(true, "com.example.android.snake", "string", "mode_lose_prefix", Integer.valueOf(3)),
            new ResourceRef(true, "com.example.android.snake", "string", "mode_lose_suffix", Integer.valueOf(4)),
            new ResourceRef(true, "com.example.android.snake", "string", "mode_pause", Integer.valueOf(5)),
            new ResourceRef(true, "com.example.android.snake", "string", "swap_currencies", Integer.valueOf(6)),
            new ResourceRef(true, "com.example.android.snake", "string", "somethingelse", Integer.valueOf(7)));
      }
    };
  }
  
  private AndroidLayoutGrammar grammar;
  
  @Before
  public void before() {
    DalvikPlatform platform = DalvikPlatformManager.getDefault().findPlatformForTarget(getPlatform().target);
    grammar = AndroidLayoutGrammar.create(platform, rr);
  }

  @Test
  public void testTags() throws Exception {
    String p = PRELUDE + "  <LinearLayout " + XMLNS + LAYOUT_ATTRS + "<THERE/>\n" +
        "  </LinearLayout>\n";

    List<String> l = TestUtil.grammarResultValues(grammar.queryElements(TestUtil.createCompletion(p)));
    assertTrue(grammar.toString(), l.contains("TextView"));
    assertFalse(grammar.toString(), l.contains("Button"));
  }

  @Test
  public void testLinearLayoutWidth() throws Exception {
    // to get layout_width  for LinearLayout we need to look at it layout attrs of its superclass
    // ViewGroup_Layout -> ViewGroup.LayoutParams
    // to get gravity for LinearLayout we need to look at its layout params
    // LinearLayout_Layout -> LinearLayout.LayoutParams
    String p = Resources.toString(AndroidLayoutGrammarTest.class.getResource("linLayoutAttrs.xml"), Charsets.UTF_8);

    List<String> l = TestUtil.grammarResultValues(grammar.queryAttributes(TestUtil.createCompletion(p)));
    assertTrue(grammar.toString(), l.contains("android:layout_width"));
    assertTrue(grammar.toString(), l.contains("android:gravity"));
  }

  @Test
  public void testLinearLayoutWidthInChild() throws Exception {
    // to get layout_width  for a child in a LinearLayout we need to look at it layout attrs of a superclass of this
    // layout ViewGroup_Layout -> ViewGroup.LayoutParams
    String p = Resources.toString(AndroidLayoutGrammarTest.class.getResource("linLayoutAttrs1.xml"), Charsets.UTF_8);
    List<String> l = TestUtil.grammarResultValues(grammar.queryAttributes(TestUtil.createCompletion(p)));
    assertTrue(grammar.toString(), l.contains("android:layout_width"));
    assertEquals("just one result " + l, 1, l.size());
  }

  @Test
  public void testTagsDoNotHaveParamStyleables() throws Exception {
    String p = PRELUDE + "  <LinearLayout " + XMLNS + LAYOUT_ATTRS + "<LinearLHERE/>\n" +
        "  </LinearLayout>\n";
    List<String> l = TestUtil.grammarResultValues(grammar.queryElements(TestUtil.createCompletion(p)));
    assertTrue(grammar.toString(), l.contains("LinearLayout"));
    assertFalse(grammar.toString(), l.contains("LinearLayout_Layout"));
    assertFalse(grammar.toString(), l.contains("android:bottom"));
  }

  @Test
  public void testTagsDoNotHaveParamStyleables2() throws Exception {
    String p = PRELUDE + "  <LinearLayout " + XMLNS + LAYOUT_ATTRS + "<aHERE/>\n" +
        "  </LinearLayout>\n";
    List<String> l = TestUtil.grammarResultValues(grammar.queryElements(TestUtil.createCompletion(p)));
    assertFalse(grammar.toString(), l.contains("android:bottom"));
  }

  @Test
  public void testAttributes() throws Exception {
    String p = PRELUDE + "  <LinearLayout " + XMLNS + LAYOUT_ATTRS + "<TextView android:HERE=\"foo\"/>\n" +
        "  </LinearLayout>\n";
    List<String> l = TestUtil.grammarResultValues(grammar.queryAttributes(TestUtil.createCompletion(p)));
    assertTrue(grammar.toString(), l.contains("android:id"));
    assertTrue(grammar.toString(), l.contains("android:layout_height"));
  }

  @Test
  public void testAttributeValues() throws Exception {
    String p = PRELUDE + "  <LinearLayout " + XMLNS + LAYOUT_ATTRS + "<TextView android:layout_width=\"HERE\"/>\n" +
        "  </LinearLayout>\n";
    List<String> l = TestUtil.grammarResultValues(grammar.queryValues(TestUtil.createCompletion(p)));
    assertTrue(grammar.toString(), l.contains("fill_parent")); // or match_parent that deprecates fill_parent since API level 8
    assertTrue(grammar.toString(), l.contains("wrap_content"));
    assertTrue(l.size() <= 3); // 2 or 3
  }

  @Test
  public void testAttributeValueEnum() throws Exception {
    String p = PRELUDE + "  <LinearLayout " + XMLNS + LAYOUT_ATTRS + "<TextView android:bufferType=\"HERE\"/>\n" +
        "  </LinearLayout>\n";
    List<String> l = TestUtil.grammarResultValues(grammar.queryValues(TestUtil.createCompletion(p)));
    assertTrue(grammar.toString(), l.contains("normal"));
    assertTrue(grammar.toString(), l.contains("editable"));
    assertTrue(grammar.toString(), l.contains("spannable"));
  }

  @Test
  public void testAttributeValuesDimensions() throws Exception {
    String p = PRELUDE + "  <LinearLayout " + XMLNS + LAYOUT_ATTRS + "<TextView android:layout_width=\"200HERE\"/>\n" +
        "  </LinearLayout>\n";
    List<String> l = TestUtil.grammarResultValues(grammar.queryValues(TestUtil.createCompletion(p)));
    for (String unit : new String[] {"dp", "sp", "pt", "mm", "in", "px"})
    assertTrue(grammar.toString(), l.contains("200" + unit));
  }

  @Test
  public void testAttributeValuesDimensions2() throws Exception {
    String p = PRELUDE + "  <LinearLayout " + XMLNS + LAYOUT_ATTRS + "<TextView android:textSize=\"15pHERE\"/>\n" +
        "  </LinearLayout>\n";
    List<String> l = TestUtil.grammarResultValues(grammar.queryValues(TestUtil.createCompletion(p)));
    for (String unit : new String[] {"pt", "px"}) {
      assertTrue(grammar.toString(), l.contains("15" + unit));
    }
  }

  @Test
  public void references() throws Exception {
    String p = Resources.toString(AndroidLayoutGrammarTest.class.getResource("linLayoutAttrs2.xml"), Charsets.UTF_8);
    p = p.replace("HERE", "@string/moHERE");
    List<String> l = TestUtil.grammarResultValues(grammar.queryValues(TestUtil.createCompletion(p)));
    assertTrue(grammar.toString(), l.contains("@string/mode_lose_prefix"));
    assertTrue(grammar.toString(), l.contains("@string/mode_lose_suffix"));
    assertTrue(grammar.toString(), l.contains("@string/mode_pause"));
    assertEquals(3, l.size());
  }

  @Test
  public void stringReferences() throws Exception {
    String p = Resources.toString(AndroidLayoutGrammarTest.class.getResource("linLayoutAttrs3.xml"), Charsets.UTF_8);
    // <Button ... android:text="@string/swap_currencies"
    p = p.replace("HERE", "@string/sHERE");
    List<String> l = TestUtil.grammarResultValues(grammar.queryValues(TestUtil.createCompletion(p)));
    assertTrue(grammar.toString(), l.contains("@string/swap_currencies"));
    assertTrue(grammar.toString(), l.contains("@string/somethingelse"));
    assertEquals(2, l.size());
  }

  @Test
  public void referenceTypes() throws Exception {
    String p = Resources.toString(AndroidLayoutGrammarTest.class.getResource("linLayoutAttrs2.xml"), Charsets.UTF_8);
    p = p.replace("HERE", "@sHERE");
    List<String> l = TestUtil.grammarResultValues(grammar.queryValues(TestUtil.createCompletion(p)));
    assertTrue(grammar.toString(), l.contains("@string/"));
    assertEquals(1, l.size());
  }

  @Test
  public void referenceTypesWithPlus() throws Exception {
    String p = Resources.toString(AndroidLayoutGrammarTest.class.getResource("linLayoutAttrs2.xml"), Charsets.UTF_8);
    p = p.replace("HERE", "@+iHERE");
    List<String> l = TestUtil.grammarResultValues(grammar.queryValues(TestUtil.createCompletion(p)));
    assertTrue(grammar.toString(), l.contains("@+id/"));
    assertEquals(1, l.size());
  }
}
