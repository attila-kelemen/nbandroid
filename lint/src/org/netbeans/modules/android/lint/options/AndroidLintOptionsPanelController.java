/*
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package org.netbeans.modules.android.lint.options;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

//@OptionsPanelController.SubRegistration(location = "Editor/Hints/text/android+x-java",
//displayName = "#AdvancedOption_DisplayName_AndroidLint",
//keywords = "#AdvancedOption_Keywords_AndroidLint",
//keywordsCategory = "Hints/AndroidLint")
//@org.openide.util.NbBundle.Messages({"AdvancedOption_DisplayName_AndroidLint=Android Lint", "AdvancedOption_Keywords_AndroidLint=android lint"})
public final class AndroidLintOptionsPanelController extends OptionsPanelController {

    private AndroidLintPanel panel;
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private boolean changed;

    public void update() {
        if (panel != null) panel.load();
        changed = false;
    }

    public void applyChanges() {
        if (panel != null) panel.store();
        changed = false;
    }

    public void cancel() {
        // need not do anything special, if no changes have been persisted yet
    }

    public boolean isValid() {
        return panel != null ? panel.valid() : true;
    }

    public boolean isChanged() {
        return changed;
    }

    public HelpCtx getHelpCtx() {
        return null; // new HelpCtx("...ID") if you have a help set
    }

    public JComponent getComponent(Lookup masterLookup) {
        if (panel == null) {
            panel = new AndroidLintPanel(this, OptionsFilterHack.create(masterLookup)/*masterLookup.lookup(OptionsFilter.class)*/);
        }
        return panel;
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        pcs.removePropertyChangeListener(l);
    }

    void changed() {
        if (!changed) {
            changed = true;
            pcs.firePropertyChange(OptionsPanelController.PROP_CHANGED, false, true);
        }
        pcs.firePropertyChange(OptionsPanelController.PROP_VALID, null, null);
    }
}
