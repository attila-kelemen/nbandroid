package org.nbandroid.netbeans.ext.navigation;

import com.android.resources.ResourceType;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.awt.Toolkit;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.ui.ElementOpen;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.modules.android.project.api.ResourceRef;
import org.openide.awt.StatusDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.xml.XMLUtil;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.ext.DefaultHandler2;

/**
 *
 * @author radim
 */
class UiUtilsCallerImpl implements UiUtilsCaller {
  private static final Logger LOG = Logger.getLogger(UiUtilsCallerImpl.class.getName());

  public interface OpenExecutor {
    boolean doOpen(FileObject fo);
    boolean doOpenLine(FileObject fo, int line);
  }

  final OpenExecutor openExecutor;

  public UiUtilsCallerImpl(OpenExecutor openExecutor) {
    this.openExecutor = openExecutor;
  }

  @Override
  public boolean open(FileObject fo, ResourceRef resRef) {
    if (fo == null) {
      return false;
    }
    Project p = FileOwnerQuery.getOwner(fo);
    if (p == null) {
      LOG.log(Level.FINE, "Cannot navigate to resource: project for {0} not found.", fo);
      return false;
    }
    ResourceLocator locator = p.getLookup().lookup(ResourceLocator.class);
    ResourceLocation location = locator != null ? locator.findResourceLocation(resRef) : null;
    if (location != null) {
      if (location.line >= 0) {
        return openExecutor.doOpenLine(location.resource, location.line);
      } else {
        return openExecutor.doOpen(location.resource);
      }
    }
    return false;
  }

  @Override
  public boolean open(ClasspathInfo cpInfo, ElementHandle<? extends Element> el) {
    return ElementOpen.open(cpInfo, el);
  }

  @Override
  public void beep() {
    Toolkit.getDefaultToolkit().beep();
    StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(GoToSupport.class, "WARN_CannotGoToGeneric"));
  }

  @Override
  public void warnCannotOpen(String displayName) {
    Toolkit.getDefaultToolkit().beep();
    StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(GoToSupport.class, "WARN_CannotGoTo", displayName));
  }
    
  private static ResourceLocation findResourcePosition(final ResourceType res, final String value, FileObject folder) {
    return Iterables.find(
        Iterables.transform(
            folder != null ? Lists.newArrayList(folder.getChildren()) : Collections.<FileObject>emptyList(), 
            new Function<FileObject, ResourceLocation>() {
              @Override
              public ResourceLocation apply(FileObject f) {
                Map<String, ResourceLocation> idResources = parseResources(f).get(res);
                return idResources != null ? idResources.get(value) : null;
              }
            }),
        Predicates.<ResourceLocation>notNull(),
        null);
  }
  
  private static Map<ResourceType, Map<String, ResourceLocation>> parseResources(final FileObject fo) {
    final Map<ResourceType, Map<String, ResourceLocation>> result = 
        new EnumMap<>(ResourceType.class);
    try {
      XMLReader reader = XMLUtil.createXMLReader();

      DefaultHandler2 handler = new DefaultHandler2() {

        private Locator locator;
        Stack<String> elements;
        String currentStyleableName = null;
        {
          this.elements = new Stack<>();
        }

        @Override
        public void setDocumentLocator(Locator locator) {
          this.locator = locator;
        }
        
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attrs) throws SAXException {
          boolean scanStrings = !elements.empty() && "resources".equals(elements.peek()) && "string".equals(qName);
          boolean scanStyleable = !elements.empty() && "resources".equals(elements.peek()) && "declare-styleable".equals(qName);
          boolean scanStyleableAttrs = !elements.empty() && "declare-styleable".equals(elements.peek()) && "attr".equals(qName);
          for (int i = 0; i < attrs.getLength(); i++) {
            if ("android:id".equals(attrs.getQName(i))) {
              String id = attrs.getValue(i);
              if (id != null && id.startsWith("@+id/")) {
                id = id.substring("@+id/".length());
                Map<String, ResourceLocation> ids = result.get(ResourceType.ID);
                if (ids == null) {
                  ids = Maps.newHashMap();
                  result.put(ResourceType.ID, ids);
                }
                ids.put(id, new ResourceLocation(fo, locator.getLineNumber()));
              }
            }
            if (scanStrings && "name".equals(attrs.getQName(i))) {
              String id = attrs.getValue(i);
              if (id != null) {
                Map<String, ResourceLocation> ids = result.get(ResourceType.STRING);
                if (ids == null) {
                  ids = Maps.newHashMap();
                  result.put(ResourceType.STRING, ids);
                }
                ids.put(id, new ResourceLocation(fo, locator.getLineNumber()));
              }
              
            }
            if (scanStyleable && "name".equals(attrs.getQName(i))) {
              String id = attrs.getValue(i);
              if (id != null) {
                Map<String, ResourceLocation> ids = result.get(ResourceType.STYLEABLE);
                if (ids == null) {
                  ids = Maps.newHashMap();
                  result.put(ResourceType.STYLEABLE, ids);
                }
                ids.put(id, new ResourceLocation(fo, locator.getLineNumber()));
              }
              currentStyleableName = id;
            }
            if (scanStyleableAttrs && "name".equals(attrs.getQName(i))) {
              String id = currentStyleableName + '_' + attrs.getValue(i);
              if (id != null) {
                Map<String, ResourceLocation> ids = result.get(ResourceType.STYLEABLE);
                if (ids == null) {
                  ids = Maps.newHashMap();
                  result.put(ResourceType.STYLEABLE, ids);
                }
                ids.put(id, new ResourceLocation(fo, locator.getLineNumber()));
              }
              currentStyleableName = id;
            }
          }
          elements.push(qName);
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
          elements.pop();
        }
        
      };
      reader.setContentHandler(handler);

      InputStream is = fo.getInputStream();
      reader.parse(new InputSource(is));
      is.close();

    } catch (SAXException | IOException ex) {
      Exceptions.printStackTrace(ex);
    }
    return result;
  }
}
