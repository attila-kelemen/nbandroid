package org.nbandroid.netbeans.ext.navigation;

import javax.swing.text.Document;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.api.xml.lexer.XMLTokenId;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;

/**
 *
 * @author radim
 */
class EditorUtilities {
  
  static FileObject getFileObject(Document doc) {
    DataObject od = (DataObject) doc.getProperty(Document.StreamDescriptionProperty);

    return od != null ? od.getPrimaryFile() : null;
  }
  // a copy of SourceUtils.getJavaTokenSequence adjusted for XML
  static TokenSequence<XMLTokenId> getXmlTokenSequence(final TokenHierarchy hierarchy, final int offset) {
    if (hierarchy != null) {
      TokenSequence<?> ts = hierarchy.tokenSequence();
      while (ts != null && (offset == 0 || ts.moveNext())) {
        ts.move(offset);
        if (ts.language() == XMLTokenId.language()) {
          return (TokenSequence<XMLTokenId>) ts;
        }
        if (!ts.moveNext() && !ts.movePrevious()) {
          return null;
        }
        ts = ts.embedded();
      }
    }
    return null;
  }
}
