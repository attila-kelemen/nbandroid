/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.netbeans.modules.android.core.sdk;

import com.google.common.collect.Iterables;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nbandroid.netbeans.test.AbstractAndroidPlatformTest;
import org.nbandroid.netbeans.test.PlatformVersions;

import static org.junit.Assert.*;
/**
 * Test functionality of AndroidGrammar.
 */
@PlatformVersions
public class LayoutClassesParserTest extends AbstractAndroidPlatformTest {
  private static final String SDK_DIR = System.getProperty("test.all.android.sdks.home");

  @BeforeClass
  public static void classSetup() {
    DalvikPlatformManager.getDefault().setSdkLocation(SDK_DIR);
  }

  @Test
  public void testAndroidLayoutsModel() throws Exception {
      DalvikPlatform platform = DalvikPlatformManager.getDefault().findPlatformForTarget(getPlatform().target);
      assertNotNull(platform);
      WidgetData data = platform.widgetDataSupplier().get();

      UIClassDescriptor clz = Iterables.getOnlyElement(UIClassDescriptors.findBySimpleName(data, "LinearLayout"));
      assertNotNull(clz);
      assertTrue(Iterables.isEmpty(
          UIClassDescriptors.findBySimpleName(data, LayoutElementType.LAYOUT_PARAM, "LinearLayout")));
      clz = Iterables.getOnlyElement(
          UIClassDescriptors.findBySimpleName(data, LayoutElementType.VIEW_GROUP, "LinearLayout"));
      assertNotNull(clz);
      assertEquals("android.widget.LinearLayout", clz.getFQClassName());
      assertEquals("android.view.ViewGroup", clz.getSuperclass());

      clz = UIClassDescriptors.findParamsForName(data, "android.widget.LinearLayout");
      assertEquals("android.widget.LinearLayout.LayoutParams", clz.getFQClassName());
      assertEquals("android.view.ViewGroup.MarginLayoutParams", clz.getSuperclass());

      UIClassDescriptor pClz = Iterables.getOnlyElement(
          UIClassDescriptors.findBySimpleName(data, LayoutElementType.LAYOUT_PARAM, "LinearLayout.LayoutParams"));
      assertNotNull(pClz);
      assertEquals("android.widget.LinearLayout.LayoutParams", pClz.getFQClassName());
      pClz = UIClassDescriptors.findByFQName(data, pClz.getSuperclass());
      assertNotNull(pClz);
      assertEquals("android.view.ViewGroup.MarginLayoutParams", pClz.getFQClassName());

      // we can find descriptors by their names
      assertNotNull(UIClassDescriptors.findByFQName(data, "android.view.ViewGroup"));
  }
}
