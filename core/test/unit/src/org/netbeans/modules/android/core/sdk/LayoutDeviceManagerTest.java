package org.netbeans.modules.android.core.sdk;

import com.android.sdklib.devices.Device;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 *
 * @author radim
 */
public class LayoutDeviceManagerTest {
  
  private static final String SDK_DIR = System.getProperty("test.all.android.sdks.home");
  
  @Test
  public void testDeviceXmlParsing() {
//    DalvikPlatformManager.getDefault().setSdkLocation(SDK_DIR);
//    
    List<Device> devices = Lists.newArrayList(DalvikPlatformManager.getDefault().getDevices());
    assertTrue(devices.size() >= 15);
  }
}
