/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.netbeans.modules.android.core.sdk;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.nbandroid.netbeans.test.AbstractAndroidPlatformTest;
import org.nbandroid.netbeans.test.PlatformVersions;
import org.netbeans.api.java.classpath.ClassPath;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;

import static org.junit.Assert.*;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

@PlatformVersions
public class DalvikPlatformCrossVersionTest extends AbstractAndroidPlatformTest {

  private static final String SDK_DIR = System.getProperty("test.all.android.sdks.home");

  private DalvikPlatformManager dpm;

  @Before
  public void setUp() {
    dpm = new DalvikPlatformManager();
    dpm.setSdkLocation(SDK_DIR);
  }
  
  private Matcher<FileObject> installFolderNameMatches(final String folderPath) {
    final String[] pathSegments = folderPath.split("/");
    return new TypeSafeMatcher<FileObject>() {

      @Override
      protected boolean matchesSafely(FileObject f) {
        return f.getParent().getNameExt().equals(pathSegments[0]) && 
            f.getNameExt().startsWith(pathSegments[1]) || f.getNameExt().startsWith(pathSegments[1] + "-");
      }

      @Override
      public void describeTo(Description d) {
        d.appendText("folder has path like " + folderPath);
      }
    };
  }

  @Test
  @PlatformVersions(type = PlatformVersions.PlatformType.PURE)
  public void cleanPlatforms() throws Exception {
    DalvikPlatform platform = dpm.findPlatformForTarget(getPlatform().target);
    assertNotNull(getPlatform().platformDir, platform);
    assertThat(platform.getInstallFolder(), installFolderNameMatches(getPlatform().installDir));
    assertThat(platform.getPlatformFolder(), installFolderNameMatches(getPlatform().platformDir));
    for (Tool t : Tool.values()) {
      assertNotNull("Tool " + t.getSystemName() + " in " + platform, platform.findTool(t.getSystemName()));
    }
    doTestClasspath(platform.getBootstrapLibraries(), "android.jar");
    doTestClasspathResource(platform.getBootstrapLibraries(), "android/app/Application.class");
    doTestClasspathResource(platform.getBootstrapLibraries(), "android/annotation/TargetApi.class");
    doTestSources(platform);
    doTestTools(platform);
  }

  @Test
  @PlatformVersions(type = PlatformVersions.PlatformType.EXTENSION)
  public void googleApiPlatforms() throws Exception {
    DalvikPlatform platform = dpm.findPlatformForTarget(getPlatform().target);
    assertNotNull(getPlatform().platformDir, platform);
    assertThat(platform.getInstallFolder(), installFolderNameMatches(getPlatform().installDir));
    assertThat(platform.getPlatformFolder(), installFolderNameMatches(getPlatform().platformDir));
    for (Tool t : Tool.values()) {
      assertNotNull("Tool " + t.getSystemName() + " in " + platform, platform.findTool(t.getSystemName()));
    }
    doTestClasspath(platform.getBootstrapLibraries(), "android.jar", "maps.jar");
    doTestClasspathResource(platform.getBootstrapLibraries(), "android/annotation/TargetApi.class");
    doTestSources(platform);
    // doTestClasspath(platform.getStandardLibraries());
    doTestTools(platform);
  }

  @Test
  @PlatformVersions(type = PlatformVersions.PlatformType.EXTENSION)
  public void javadocs() throws Exception {
    DalvikPlatform platform = dpm.findPlatformForTarget(getPlatform().target);
    // Newer platforms have sources and javadoc disappeared
    // doTestJavadoc(platform, "android/app/Activity.html");
    doTestJavadoc(platform, "com/google/android/maps/MapView.html");
  }

  /** Tests bootclasspath */
  private void doTestClasspath(List<URL> cp, String ... jars) {
    assertTrue(cp != null && !cp.isEmpty());
    Set<String> jarNames = new HashSet<>();
    Collections.addAll(jarNames, jars);
    for (URL cpEntry : cp) {
      if (FileUtil.getArchiveFile(cpEntry) != null) {
        jarNames.remove(URLMapper.findFileObject(FileUtil.getArchiveFile(cpEntry)).getNameExt());
      }
    }
    assertTrue("No missing libraries in " + jarNames, jarNames.isEmpty());
  }

  /** Tests bootclasspath */
  private void doTestClasspathResource(List<URL> cp, String classfileName) {
    assertTrue(cp != null && !cp.isEmpty());
    for (URL cpEntry : cp) {
      final URL archiveFile = FileUtil.getArchiveFile(cpEntry);
      if (archiveFile != null) {
        if (URLMapper.findFileObject(cpEntry).getFileObject(classfileName) != null) {
          return;
        }
      }
    }
    fail("Cannot find " + classfileName + " in classpath " + cp);
  }

  private void doTestSources(DalvikPlatform platform) {
    int apiLevel = platform.getAndroidTarget().getVersion().getApiLevel();
    if (apiLevel >= 14) {
      final ClassPath sourceCP = platform.getSourceFolders();
      FileObject javadocFo = sourceCP.findResource("android/app/Activity.java");
      assertNotNull("source for Activity in " + sourceCP + " for platform " + platform, 
          javadocFo);
    }
  }
  
  private void doTestTools(DalvikPlatform platform) {
    for (Tool t : Util.sdkTools()) {
      FileObject toolFO = platform.findTool(t.getSystemName());
      assertNotNull(t.getSystemName() + " found", toolFO);
      assertTrue(t.getSystemName() + " valid", toolFO.isValid());
    }
  }

  private void doTestJavadoc(DalvikPlatform platform, String resource)
      throws MalformedURLException {
    List<URL> javadocs = platform.getJavadocFolders();
    assertNotNull(javadocs);
    boolean found = false;
    for (URL docRoot : javadocs) {
      FileObject documentationFO = URLMapper.findFileObject(new URL(docRoot, resource));
      if (documentationFO != null && documentationFO.isValid()) {
        found = true;
        break;
      }
    }
    assertTrue("Resource " + resource + " found in javadoc", found);
  }
}
