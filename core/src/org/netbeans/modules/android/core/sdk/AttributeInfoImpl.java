/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.netbeans.modules.android.core.sdk;

import com.android.utils.Pair;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

/**
 * Object that can be included in Android XML file to describe view or view layout element.
 *
 * @author radim
 */
class AttributeInfoImpl implements AttributeInfo {

  private final String name;
  /*Nullable*/
  private final String description;
  private final Set<Format> formats = Sets.newHashSet();
  /** Pairs of flag name and its value (mask). */
  private final Set<Pair<String, Integer>> flags = Sets.newTreeSet(new Comparator<Pair<String, Integer>>() {

    @Override
    public int compare(Pair<String, Integer> o1, Pair<String, Integer> o2) {
      return o1.getFirst().compareTo(o2.getFirst());
    }
  });
  /** Pairs of enum and its value. */
  private final List<Pair<String, Integer>> enums = Lists.newArrayList();

  public AttributeInfoImpl(String name, String description) {
    this.name = Preconditions.checkNotNull(name);
    this.description = description;
  }

  void addEnum(Pair<String, Integer> enumValue) {
    enums.add(enumValue);
  }

  void addFlag(Pair<String, Integer> flagValue) {
    flags.add(flagValue);
  }

  void addFormat(Format format) {
    formats.add(format);
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public Iterable<Format> getFormats() {
    return Collections.unmodifiableSet(formats);
  }

  @Override
  public Iterable<Pair<String, Integer>> getEnumValues() {
    return Collections.unmodifiableList(enums);
  }

  @Override
  public Iterable<Pair<String, Integer>> getFlagValues() {
    return Collections.unmodifiableSet(flags);
  }

  @Override
  public String toString() {
    return "AttributeInfo{" +
        "name=" + name +
        // ", description=" + description +
        ", formats=" + formats +
        ", enums=" + enums +
        ", flags=" + flags + '}';
  }

}
